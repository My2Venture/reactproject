﻿using ReactApp.Domain.Data.Repositories.Interfaces;
using ReactApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Domain.Data.Repositories
{
    public class PortRepository : Repository<Port>, IPortRepository
    {
        public PortRepository(ReactContext context) : base(context)
        {

        }

        public ReactContext ReactEntities
        {
            get { return _context as ReactContext; }
        }
    }
}
