﻿using ReactApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Services
{
    public interface IShipService
    {
        Ship CreateShip(Ship model);
        Ship EditShip(Ship model);
        bool DeleteShip(Ship model);

        IEnumerable<Ship> GetAllShips();
    }
}
