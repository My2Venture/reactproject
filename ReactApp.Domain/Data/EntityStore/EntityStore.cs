﻿using ReactApp.Domain.Data.Repositories;
using ReactApp.Domain.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Domain.Data.EntityStore
{
    public class EntityStore : IEntityStore
    {
        private readonly ReactContext _context;

        public EntityStore(ReactContext context)
        {
            _context = context;

            Ships = new ShipRepository(_context);
        }

        public IShipRepository Ships { get; private set; }
        public IPortRepository Ports { get; private set; }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }

}
