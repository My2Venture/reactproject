﻿using ReactApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Domain.Data.Repositories.Interfaces
{
    public interface IPortRepository : IRepository<Port>
    {
    }
}
