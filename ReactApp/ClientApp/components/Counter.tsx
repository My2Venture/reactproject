import * as React from 'react';
import { RouteComponentProps } from 'react-router';

interface CounterState {
    currentCount: number;
    cache: string;
}

export class Counter extends React.Component<RouteComponentProps<{}>, CounterState> {
    constructor() {
        super();
        this.state = { currentCount: 0, cache: "" };
    }

    public render() {
        return <div>
            <h1>Counter</h1>

            <p>This is a simple example of a React component.</p>

            <p>Current count: <strong>{ this.state.currentCount }</strong></p>

            <button onClick={() => { this.incrementCounter() }}>Increment</button>

            <button onClick={() => { this.sendCache() }}>Cache Set</button>

            <button onClick={() => { this.getCache() }}>Cache Get</button>

            <p>This is in the cache! <strong> {this.state.cache}</strong></p>
        </div>;
    }

    incrementCounter() {
        this.setState({
            currentCount: this.state.currentCount + 1
        });
    }

    sendCache() {
        fetch('api/sampledata/cachetest', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify( this.state.currentCount )
           
        })
    }

    getCache() {
        fetch("api/sampledata/cachetest")
            .then((response) => response.json())
            .then((responseJson) => this.setState({ cache: responseJson }))
    }
}
