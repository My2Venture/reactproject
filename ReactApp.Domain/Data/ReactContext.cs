﻿using Microsoft.EntityFrameworkCore;
using ReactApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Domain.Data
{
    public class ReactContext : DbContext
    {
        public ReactContext(DbContextOptions<ReactContext> options)
            : base(options)
        { }

        public DbSet<Ship> Ships { get; set; }
        public DbSet<Port> Ports { get; set; }
    }
}
