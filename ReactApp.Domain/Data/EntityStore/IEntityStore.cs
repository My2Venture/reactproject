﻿using ReactApp.Domain.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Domain.Data.EntityStore
{
    public interface IEntityStore
    {
        IShipRepository Ships { get; }
        IPortRepository Ports { get; }

        Task<int> Complete();
        void Dispose();
    }
}
