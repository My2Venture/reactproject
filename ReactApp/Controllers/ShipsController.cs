﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using ReactApp.Domain.Data.EntityStore;
using ReactApp.Domain.Models;
using ReactApp.Services;

namespace ReactApp.Controllers
{
    [Route("api/[controller]/")]
    public class ShipsController : Controller
    {
        
        private IMemoryCache _cache;
        private readonly IShipService _service;

        public ShipsController(IMemoryCache cache, IShipService service)
        {
            _cache = cache;           
            _service = service;
        }

        [HttpGet]
        public JsonResult Index()
        {
            IEnumerable<Ship> ships = _service.GetAllShips();

            var shipsOutput = _cache.Get<Ship>("ShipsKey");

            return Json(shipsOutput);
        }

        [HttpPost]
        public Ship Post([FromBody] Ship model)
        {
            Ship createdShip = _service.CreateShip(model);

            _cache.Set("ShipKey", model);
            
            return createdShip;
        }

        [HttpPut]
        public JsonResult Put([FromBody] Ship model)
        {
            Ship updatedShip = _service.EditShip(model);

            _cache.Set("Ships", model);

            return Json(updatedShip);
        }

        [HttpDelete]
        public string Delete(Ship model)
        {

            var success = _service.DeleteShip(model);
            _cache.Set("ships", _service.GetAllShips());

            return "Ok";
        }

    }
}