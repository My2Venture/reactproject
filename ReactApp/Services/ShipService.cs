﻿using Microsoft.Extensions.Caching.Memory;
using ReactApp.Domain.Data.EntityStore;
using ReactApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactApp.Services
{
    public class ShipService : IShipService
    {
        private readonly IEntityStore _store;
        private readonly IMemoryCache _cache;

        public ShipService(IEntityStore store, IMemoryCache cache)
        {
            _store = store;
        }

        public Ship CreateShip(Ship model)
        {
            Ship ship = model;

            _store.Ships.Add(ship);

            _cache.Set("Ships", _store.Ships);

            _store.Complete();

            return ship;
        }

        public Ship EditShip(Ship model)
        {
            Ship ship = model;

            _store.Ships.Update(ship);

            _cache.Set("Ships", _store.Ships);

            _store.Complete();

            return ship;
        }

        public bool DeleteShip(Ship model)
        {
            _store.Ships.Remove(model);

            return true;
        }
        

        public IEnumerable<Ship> GetAllShips()
        {
            return _store.Ships.GetAll();
        }
    }
}
