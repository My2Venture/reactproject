using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace ReactApp.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private IMemoryCache _cache;

        public SampleDataController(IMemoryCache cache)
        {
            _cache = cache;
        }

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        [HttpGet]
        [Route("cacheTest")]
        public JsonResult Get()
        {
            var cacheOutput = _cache.Get<int>("IDGKey");
            return Json(cacheOutput);
        }

        [HttpPost]
        [Route("cachetest")]
        public string Post([FromBody] int firstParam)
        {
            _cache.Set("IDGKey", firstParam);

            return "Fine";
        }


        [HttpGet]
        [Route("weathertest")]
        public JsonResult GetWeather()
        {
            var cacheOutput = _cache.Get<WeatherForecast[]>("WeatherKey");
            return Json(cacheOutput);
        }

        [HttpPost]
        [Route("weathertest")]
        public string PostWeather([FromBody] WeatherForecast[] firstParam)
        {
            _cache.Set("WeatherKey", firstParam);

            return "Fine";
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
